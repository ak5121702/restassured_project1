Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Tue, 27 Feb 2024 18:03:00 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"39","createdAt":"2024-02-27T18:02:59.944Z"}