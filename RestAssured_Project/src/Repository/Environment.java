package Repository;

public class Environment {

	public static  String Hostname () {

		String hostname = "https://reqres.in/";
		return hostname;
	}
	
	public static  String Resource_post () {
		String resource = "api/users";
		return resource;
		}
	

	public static  String Resource_put () {
		String resource = "api/users/2";
		return resource;
		}
	
	public static  String Resource_get () {
		String resource = "api/users?page=2";
		return resource;
		}
	
	public static  String Resource_patch () {
		String resource = "api/users/2";
		return resource;
		}
	public static  String Resource_delete () {
		String resource = "api/users/2";
		return resource;
		}
	
	
	public static  String Headername() {
		String headername = "Content-Type";
		return headername;
		}
	
	public static  String Headervalue() {
		String headervalue = "application/json";
		return headervalue;
		}
	
	

}
