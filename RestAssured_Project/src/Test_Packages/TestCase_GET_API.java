package Test_Packages;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.testng.Assert;
import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class TestCase_GET_API extends RequestBody {

	
	public static void executor() throws ClassNotFoundException, IOException {

		File dir_name = Utility.CreateLogDirectory("Get_API_Logs");
		//String requestBody = RequestBody.req_testcase4("Get_TC1");
		String Endpoint = RequestBody.Hostname() + RequestBody.Resource_get ();
		int statuscode=0;

		for (int i = 0; i < 5; i++) {
		Response response = API_Trigger.GET_trigger(RequestBody.Headername(), RequestBody.Headervalue(),
				 Endpoint);
		statuscode = response.statusCode();

		if (statuscode == 200) {
		
		Utility.evidenceGetFile(Utility.testLogName("Get_TC1"), dir_name, Endpoint,
				response.getHeader("Date"), response.getBody().asString());
		validator(response);
		break;
	}
	else {
		System.out.println("Expected status code is not found in current iteration :" +i+ " hence retrying");
	}

}

if (statuscode!=200) {
	System.out.println("Expected status code not found even after 5 retries hence failing the test case");
	Assert.assertEquals(statuscode, 201);
}
}

	public static void validator(Response response) {

			String res_body = response.getBody().asString();
			System.out.println(response.getBody().asString());
			int statusCode = response.getStatusCode();
			System.out.println("statusCode is:" + " " + statusCode);

			JsonPath res_jsn = new JsonPath(res_body);
			int count = res_jsn.getInt("data.size()");
			System.out.println("count of data array from response:" + " " + count);

   //	declare arrays of expected data

			int id[] = { 7, 8, 9, 10, 11, 12 };
			String email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
					"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
			String firstname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
			String lastname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };



			int id_Arr[] = new int[count];
			String email_Arr[] = new String[count];
			String firstname_Arr[] = new String[count];
			String lastname_Arr[] = new String[count];


			for (int i = 0; i < count; i++) {
				{
					System.out.println("\r\n" + "-fetched array data from arrays of expected data-");

					int res_Id = res_jsn.getInt("data[" + i + "].id");
					System.out.println("\n" + res_Id);
					id_Arr[i] = res_Id;

					String res_Email = res_jsn.get("data[" + i + "].email");
					System.out.println(res_Email);
					email_Arr[i] = res_Email;

					String res_FirstName = res_jsn.getString("data[" + i + "].first_name");
					System.out.println(res_FirstName);
					firstname_Arr[i] = res_FirstName;

					String res_LastName = res_jsn.getString("data[" + i + "].last_name");
					System.out.println(res_LastName);
					lastname_Arr[i] = res_LastName;
				}


				System.out.println("-data from fetched data arrays to validate with expected array-");
				System.out.println(id_Arr[i]);
				System.out.println(email_Arr[i]);
				System.out.println(firstname_Arr[i]);
				System.out.println(lastname_Arr[i]);


				Assert.assertEquals(id[i], id_Arr[i]);
				Assert.assertEquals(email[i], email_Arr[i]);
				Assert.assertEquals(firstname[i], firstname_Arr[i]);
				Assert.assertEquals(lastname[i], lastname_Arr[i]);
		    	Assert.assertEquals(statusCode, 200);
			}
		}
	}
		