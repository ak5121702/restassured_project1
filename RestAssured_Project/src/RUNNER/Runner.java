package RUNNER;

import java.io.IOException;

import Common_Methods.Utility;
import Test_Packages.TestCase_DELETE_API;
import Test_Packages.TestCase_GET_API;
import Test_Packages.TestCase_PATCH_API;
import Test_Packages.TestCase_POST_API;
import Test_Packages.TestCase_PUT_API;

public class Runner {

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		TestCase_POST_API.executor();
		TestCase_PUT_API.executor();
		TestCase_GET_API.executor();
		TestCase_PATCH_API.executor();
		TestCase_DELETE_API.executor();
}}

